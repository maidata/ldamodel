# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 13:17:14 2023

@author: Maira Alejandra
"""

import os
import re
import numpy as np
import pandas as pd
from pprint import pprint
import warnings
from sources.mongoDB import *
from unidecode import unidecode


# Gensim
import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel
from gensim.models import CoherenceModel, LdaModel, LsiModel, HdpModel
warnings.filterwarnings('ignore')

# spacy para lematizar
import spacy

# herramientas de dibujado
import pyLDAvis.gensim_models as gensimvis
import pyLDAvis
import matplotlib.pyplot as plt


import openai
from transformers import GPT2Tokenizer

from docx import Document

# =============================================================================
# Limpieza de los datos
# =============================================================================

def cleanFunction(x):
    try:
        return x.split(':')[1]
    except:
        return ''
    
def dataReading(filename, type_):

    if type_ == True:
        df = pd.read_excel(filename+'.xlsx')
    elif type_ == False:  
        df = pd.read_excel(filename+'.csv')
        
    df['username'] = df.message.map(lambda x : str(x).split(':')[0])
    df['message'] = df.message.map(lambda x : cleanFunction(x))
    df['text_clean'] = df.message.map(lambda x: lemmatize_corpus(x))
    
    return df
    
# =============================================================================
# Generador para obtener los documentos del corpus línea a línea desde el archivo 
# del conjunto de ejemplo y convertirlos en un listado de tokens
# =============================================================================



# def lemmatize_corpus(text, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV', 'PROPN']):
#     """Función que devuelve el lema de una string,
#     excluyendo las palabras cuyo POS_TAG no está en la lista"""
#     patron = r'http\S+|www\S+'
#     text = re.sub(patron, '', text) # Eliminar enlaces
#     text = re.sub(r'@\w+', '', text) # Eliminar menciones
#     text = unidecode(text)
#     text_out = [t.lemma_.lower() for t in nlp(text)
#                 if t.pos_ in allowed_postags
#                 and len(t.lemma_)>3
#                 and not t.is_stop]
#     return text_out

list_other_stop = ['dic', 'diciembre', 'venezuela', 'mas', 'sr', 'senor', 'dios', 'the', 'of', 'and', 'you', 'si', 'that', 'to', 'in', 'is', 'who', 'it', 'a', 'acá', 'ahí', 'ajena/o/s', 'al', 'algo', 'algún', 'algun', 'alguna', 'alguno', 'algunas', 'algunos', 'allá', 'allí', 'alla', 'alli', 'ambos', 'ante', 'antes', 'aquel', 'aquella', 'aquello', 'aquellas', 'aquellos', 'aquí', 'arriba', 'así', 'atrás', 'aun', 'aunque', 'bajo', 'bastante', 'bien', 'cabe', 'cada', 'casi', 'cierto', 'cierta', 'ciertos', 'ciertas', 'como', 'con', 'conmigo', 'conseguimos', 'conseguir', 'consigo', 'consigue', 'consiguen', 'consigues', 'contigo', 'contra', 'cual', 'cuales', 'cualquier', 'cualquiera', 'cuan', 'cuando', 'cuanto', 'cuanta', 'cuantos', 'cuantas', 'de', 'dejar', 'del', 'demás', 'demasiada', 'demasiado', 'demasiadas', 'demasiados', 'dentro', 'desde', 'donde', 'dos', 'el', 'él', 'ella', 'ello', 'ellas', 'ellos', 'empleáis', 'emplean', 'emplear', 'empleas', 'empleo', 'en', 'encima', 'entonces', 'entre', 'era', 'eras', 'eramos', 'eran', 'eres', 'es', 'esa', 'ese', 'eso', 'esas', 'eses', 'esos', 'esta/s', 'estaba', 'estado', 'estáis', 'estamos', 'están', 'estar', 'este', 'esta', 'esto', 'estos', 'estas', 'estoy', 'etc', 'fin', 'fue', 'fueron', 'fui', 'fuimos', 'gueno', 'ha', 'hace', 'haces', 'hacéis', 'hacemos', 'hacen', 'hacer', 'hacia', 'hago', 'hasta', 'incluso', 'intentas', 'intenta', 'intentáis', 'intentamos', 'intentan', 'intentar', 'intento', 'ir', 'jamás', 'juntos', 'juntas', 'junto', 'junta', 'la', 'lo', 'las', 'los', 'largo', 'más', 'me', 'menos', 'mis', 'mi', 'mía', 'mias', 'mías', 'mientras', 'míos', 'mío', 'mios', 'mias', 'mías', 'mía', 'misma', 'mismo', 'mismas', 'mismos', 'modo', 'muchas', 'mucha', 'muchísima', 'muchísimo', 'muchísimas', 'muchísimos', 'mucho', 'muchos', 'muy', 'nada', 'ni', 'ningún', 'ninguna', 'ninguno', 'ningunas', 'ningunos', 'no', 'nos', 'nosotras', 'nosotros', 'nuestra', 'nuestro', 'nuestras', 'nuestros', 'nunca', 'os', 'otra', 'otro', 'otras', 'otros', 'para', 'parecer', 'pero', 'poca', 'poco', 'pocas', 'pocos', 'podéis', 'podemos', 'poder', 'podrías', 'podría', 'podríais', 'podríamos', 'podrían', 'por', 'por qué', 'porque', 'primero', 'pueden', 'puede', 'puedo', 'pues', 'que', 'qué', 'querer', 'quién', 'quien', 'quiénes', 'quienes', 'quienesquiera', 'quienquiera', 'quizás', 'quizá', 'quizas', 'quiza', 'sabe', 'sabes', 'saben', 'sabéis', 'sabemos', 'saber', 'se', 'según', 'ser', 'si', 'sí', 'siempre', 'siendo', 'sin', 'sino', 'so', 'sobre', 'sois', 'solamente', 'solo', 'sólo', 'somos', 'soy', 'sr', 'sra', 'sres', 'sta', 'sus', 'su', 'suya', 'suyo', 'suyas', 'suyos', 'tales', 'tal', 'también', 'tambien', 'tampoco', 'tan', 'tanta', 'tanto', 'tantas', 'tantos', 'te', 'tenéis', 'tenemos', 'tener', 'tengo', 'ti', 'tiempo', 'tiene', 'tienen', 'toda', 'todo', 'todas', 'todos', 'tomar', 'trabajáis', 'trabajamos', 'trabajan', 'trabajar', 'trabajas', 'tras', 'tú', 'tu', 'tus', 'tuya', 'tuyo', 'tuyas', 'tuyos', 'último', 'ultimo', 'una', 'uno', 'unas', 'unos', 'usa', 'usas', 'usáis', 'usamos', 'usan', 'usar', 'uso', 'usted', 'ustedes', 'va', 'van', 'vais', 'valor', 'vamos', 'varias', 'varios', 'vaya', 'verdadera', 'vosotras', 'vosotros', 'voy', 'vuestra', 'vuestro', 'vuestras', 'vuestros', 'y', 'ya', 'yo']
nlp = spacy.load('es_core_news_md', disable=['parser', 'ner'])
stop_words = nlp.Defaults.stop_words #listado de stop-words
list_all_stop_words = list(stop_words) + list_other_stop



def lemmatize_corpus2(text, i, list_other_stop, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV', 'PROPN']):
    """Función que devuelve el lema de una string,
    excluyendo las palabras cuyo POS_TAG no está en la lista"""
    
    # print(i)
      
    patron = r'http\S+|www\S+'
    text = re.sub(patron, '', text) # Eliminar enlaces
    text = re.sub(r'@\w+', '', text) # Eliminar menciones
    text = re.sub(r'#+', '', text) # Eliminar #
    text = unidecode(text).lower()
#     text = " ".join([word for word in text.split() if word not in list_all_stop_words])
    text_out = [t.lemma_.lower() for t in nlp(text)
                    if t.pos_ in allowed_postags
                    and len(t.lemma_)>3
                    and not t.is_stop
                    and t.lemma_.lower() not in list_other_stop
                    ]
    return text_out

# =============================================================================
# Bigramas y Trigramas
# =============================================================================

def makeBigramTrigram(df):

    bigram = gensim.models.Phrases(df.text_clean.to_list(), min_count=5, threshold=50)
    bigram_mod = gensim.models.phrases.Phraser(bigram)

    trigram = gensim.models.Phrases(bigram_mod[df.text_clean.to_list()], min_count=5, threshold=50)  
    trigram_mod = gensim.models.phrases.Phraser(trigram)

    def make_trigrams(text):
        '''Devuelve un documento convertido en trigramas según el
        modelo trigram_mod. La entrada tiene que ser una lista
        de de tokens'''
        return trigram_mod[bigram_mod[text]]

    def make_trigrams_corpus(corpus):
        '''Devuelve un documento convertido en trigramas según el
        modelo trigram_mod. La entrada tiene que ser una lista
        de documentos (como lista de tokens)'''
        return [make_trigrams(doc) for doc in corpus]

    textos_trigramas = make_trigrams_corpus(df.text_clean.to_list())
    
    return textos_trigramas

# =============================================================================
# Para seleccionar el número óptimo de temas, debemos hacer un barrido y seleccionar 
# el modelo con mayor valor de coherencia (Topic coherence). Lo podemos automatizar 
# en una función (nota: *tarda bastante en ejecutarse*).
# =============================================================================


# def evaluate_graph(dictionary, corpus, texts, limit, start=1, step=1, iter_topic=5):
#     """
#     Function to display num_topics - LDA graph using c_v coherence
    
#     Parameters:
#     ----------
#     dictionary : Gensim dictionary
#     corpus : Gensim corpus
#     limit : topic limit
#     start: min number of topics
#     step: step between topics number swept
    
#     Returns:
#     -------
#     lm_list : List of LDA topic models
#     c_v : Coherence values corresponding to the LDA model with respective number of topics
#     """
#     c_v = []
#     lm_list = []
#     n_topics = list(range(start, limit, step))
#     for num_topics in n_topics:
        
#         list_temp = []
#         for i in range(iter_topic):

#             lm = LdaModel(corpus=corpus, num_topics=num_topics, id2word=dictionary, iterations=5000)
#             # lm = LdaModel(corpus=corpus, num_topics=num_topics, id2word=corpus.diccionario)
#             lm_list.append(lm)
#             cm = CoherenceModel(model=lm, texts=texts, dictionary=dictionary, coherence='c_v')
#             list_temp.append(cm.get_coherence())
        
#         c_v.append(max(list_temp))
        
#     # Show graph
#     x = n_topics
#     plt.plot(x, c_v)
#     plt.xlabel("num_topics")
#     plt.ylabel("Coherence score")
#     plt.legend(("c_v"), loc='best')
#     plt.show()
    
#     return lm_list, c_v, n_topics


# =============================================================================
# Determinar el tema dominante en cada documento
# =============================================================================

def format_topics_sentences(ldamodel, corpus):
    # inicializa salida
    # sent_topics_df = pd.DataFrame()
    sent_topics_df = []

    # obtiene main topic de cada documento
    for row in ldamodel[corpus]:
        row = sorted(row, key=lambda x: (x[1]), reverse=True)
        # Get the Dominant topic, Perc Contribution and Keywords for each document
        (topic_num, prop_topic)=row[0]
        wp = ldamodel.show_topic(topic_num)
        topic_keywords = ", ".join([word for word, prop in wp])
        sent_topics_df.append(pd.Series([int(topic_num), round(prop_topic,4), topic_keywords]))
    
    sent_topics_df = pd.DataFrame(sent_topics_df)
    sent_topics_df.reset_index(inplace=True)
    sent_topics_df.columns = ['old_index','Tema_dominante', 'Contribucion_per', 'Palabras_clave']
    sent_topics_df['Tema_dominante'] = sent_topics_df['Tema_dominante'].astype('int')
    return(sent_topics_df)


def constructionDFChatGPT(df, df_topic_sents_keywords):

    df1 = pd.concat([df, df_topic_sents_keywords[['Tema_dominante', 'Contribucion_per', 'Palabras_clave']]], axis = 1)
    df2 = pd.DataFrame()

    for i in df1.Tema_dominante.unique():
        dff = df1[(df1.Tema_dominante == i) & (df1.Contribucion_per > df1.Contribucion_per.mean())].sort_values(ascending = False, by = 'Contribucion_per') 
        df2 = pd.concat([df1,dff[['Tema_dominante', 'message']]], axis = 0)

    df3 = df2.groupby(['Tema_dominante', 'Palabras_clave'])['message'].sum().reset_index()
    return df3

# =============================================================================
# Cálculo de Tokens para ChatGPT
# =============================================================================

def tokenizerChatGPT(row, i):

    # Inicializar el tokenizador de GPT-3.5
    tokenizer = GPT2Tokenizer.from_pretrained("gpt2")

    # # Texto de ejemplo
    # text = "Hola, ¿cómo estás?"

    # Tokenizar el texto
    tokens = tokenizer.tokenize(row)

    # Imprimir los tokens
    return len(tokens)

def optimalTokenCalculation(df):

    df['Tokens chatGPT'] = [tokenizerChatGPT(x, i) for i, x in enumerate(df.message.to_list())]
    df['Tokens'] = [len(x.split()) for x in df.message]
    df['Proporción'] = df['Tokens chatGPT'] / 32000
    df['Tokens requeridos'] = df['Tokens'] / df['Proporción']
    tokens = int(df['Tokens requeridos'].mean())
    return tokens

# =============================================================================
# Consultas a ChatGPT
# =============================================================================
api_key = "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"
openai.api_key = api_key 
MODEL_GPT = 'gpt-4-1106-preview'

def classifyTopicChatGPT(row, i, tokens_mean):
    
    try:
        prompt = f""" Te voy a proporcionar un texto, tu tarea es generar un título preciso de máximo seis palabras que mejor represente lo que esta escrito en el texto. El texto es:  

        {' '.join(row.split()[:tokens_mean])} 

        """

    #     openai.api_key = random.choice(settings.LIST_OPENAI_KEY)
        completion = openai.ChatCompletion.create(
            model=MODEL_GPT, messages=[{"role": "user", "content": prompt}], temperature=0
        )
            
        return completion["choices"][0]["message"]["content"]
    
    except:
        return i

def classifySummaryChatGPT(row, i, tokens_mean):
    
    try:

        prompt = f""" Te voy a proporcionar un texto, tu tarea es generar un resumen coherente del contenido de este texto donde se resalte el tema principal del texto proporcionado y los temas subyasentes de este texto. El texto es:  

        {' '.join(row.split()[:tokens_mean])} 
        """

    #     openai.api_key = random.choice(settings.LIST_OPENAI_KEY)
        completion = openai.ChatCompletion.create(
            model=MODEL_GPT, messages=[{"role": "user", "content": prompt}], temperature=0
        )
        return completion["choices"][0]["message"]["content"]
    
    except:
        return i
    
def classifyWordsChatGPT(row, keywords_, i, tokens_mean):
    
    try:
 
        prompt = f""" Te voy a proporcionar un texto y las palabras claves más relevantes de ese texto, tu tarea es generar un análisis semántico de estas palabras clave más relevantes en el texto. El texto es:  

        {' '.join(row.split()[:tokens_mean])} 
        
        y las palabras claves más relevantes de este texto son:
        
        {keywords_} 

        """

    #     openai.api_key = random.choice(settings.LIST_OPENAI_KEY)
        completion = openai.ChatCompletion.create(
            model=MODEL_GPT, messages=[{"role": "user", "content": prompt}], temperature=0
        )
        return completion["choices"][0]["message"]["content"]
    
    except:
        return i 
    
# =============================================================================
# Visualización en pantalla de la consulta a chatGPT 
# =============================================================================   
def informChatGPT(df, topics, summary, keywords, filename):

    # Crear un documento de Word
    doc = Document()

    # Agregar los datos al documento
    for i in range(len(topics)):
        doc.add_paragraph(f"Tema: {topics[i]}")
        doc.add_paragraph(f"Resumen: {summary[i]}")
        doc.add_paragraph(f"Análisis de las palabras clave más relevantes y su relación con el texto: {keywords[i]}")
        doc.add_paragraph()

    # Guardar el documento
    doc.save(filename +".docx")

def documentGenerator(df, tokens_mean, filename):
    topics = [classifyTopicChatGPT(df.message[i], i, tokens_mean) for i in df.index]
    summary = [classifySummaryChatGPT(df.message[i], i, tokens_mean) for i in df.index]
    keywords = [classifyWordsChatGPT(df.message[i], df.Palabras_clave[i], i, tokens_mean) for i in df.index]
    for i in range(len(topics)):
        logger.warning(f'\nTema: "{topics[i].upper()}"\n\n')
        logger.info(f'\nResumen: {summary[i]}\n\nAnálisis de las palabras clave más relevantes y su relación con el texto: {keywords[i]}\n\n')
    
    informChatGPT(df, topics, summary, keywords, filename)
    return topics
