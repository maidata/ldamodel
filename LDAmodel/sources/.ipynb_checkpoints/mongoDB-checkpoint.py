#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 25 16:49:26 2023

@author: jhonnyrv
"""

import pymongo
from pymongo import MongoClient
import urllib
import json
import pandas as pd
from sources.configLogger import *

class Conexion:

    """
    Conexión con la base de datos mongo en la nube
    """
    def __init__(self):
        self.username = 'nucleo.analitico'
        self.my_password = '8y$fMBTAw7pMx@2K'
        self.client = None
        self.database = None
        self.collection_name = None
        self.df = None

    def auth_mongo(self, database:str):
        """
        Autentificación con la base de datos.

        ###**Parameters:**
        ----------
        * **database (String):**
            nombre de la base de datos con la que se desea conectar
        
        ###**Returns:**
        ----------
        * **(String):**
            mensaje para notificarle al usuario si se pudo conectar o no a la base de datos.

        ###**Example:**
        -----------
        ```python
        >>> import nucleoanalitico as na 
        >>> session = na.Conexion()
        >>> session.auth_mongo('Nacional')
        ```    
        """
        self.database = database
        # Conexion con BD NoSQL MongoDB - Creacion de BD y Colección

        username_url = urllib.parse.quote_plus(self.username)
        password_url = urllib.parse.quote_plus(self.my_password)

        # Fuera de GCP - Direccion Ip Externa
        # 35.225.179.227 
        # Dentro de GCP - Direccion Ip Interna
        # 10.128.0.8 
        url = 'mongodb://%s:%s@35.225.179.227:64878/'+self.database
        try: 
            self.client = MongoClient(url % (username_url, password_url))
            logger.info("Conectado satisfactoriamente!!!") 
        except:   
            logger.warning("No se logro conectar a MongoDB")
        #return client

    def insert_mongo (self, collection_name, df):
        """
        Permite insertar un dataframe a la base de datos en mongo

        ###**Parameters:**
        ----------
        * **collection_name (String):**
            nombre de la colección a la que se desea insertar los datos o la que se desea crear
        * **df (DataFrame):**
            dataframe que se desea insertar
        
        ###**Returns:**
        ----------
        * **(String):**
            mensaje para notificarle al usuario si se pudo insertar o no los datos a la colección.

        ###**Example:**
        -----------
        ```python
        >>> import nucleoanalitico as na 
        >>> session = na.Conexion()
        >>> session.auth_mongo('name_db')
        >>> session.insert_mongo('name_collection', df)
        ```   
        """
        self.collection_name = collection_name

        try:
            db = self.client[self.database]
            collection = db[self.collection_name]
            data_dict = df.to_dict("records")
            collection.insert_many(data_dict)
            logger.info("Los datos fueron insertados correctamente en la coleccion %s, de la base de datos %s." %(self.collection_name, self.database))
        
        except:
            logger.warning("Los datos NO fueron insertados en la coleccion %s, de la base de datos %s." %(collection_name, self.database))
            
    def insert_dict_to_mongo (self, collection_name, data_dict):
        """
        Permite insertar dictionario, estructura de datos para python en mongoDB

        ###**Parameters:**
        ----------
        * **collection_name (String):**
            nombre de la colección a la que se desea insertar los datos o la que se desea crear
        * **data_dict (dict):**
            diccionario que se desea insertar
        
        ###**Returns:**
        ----------
        * **(String):**
            mensaje para notificarle al usuario si se pudo insertar o no los datos a la colección.

        ###**Example:**
        -----------
        ```python
        >>> import nucleoanalitico as na 
        >>> session = na.Conexion()
        >>> session.auth_mongo('name_db')
        >>> session.insert_dict_to_mongo('name_collection', dict)
        ```   
        """
        self.collection_name = collection_name

        try:
            db = self.client[self.database]
            collection = db[self.collection_name]
            collection.insert_many(data_dict)
            logger.info("Los datos fueron insertados correctamente en la coleccion %s, de la base de datos %s." %(self.collection_name, self.database))
        
        except:
            logger.warning("Los datos NO fueron insertados en la coleccion %s, de la base de datos %s." %(self.collection_name, self.database))
       
    def insert_json_to_mongo (self, collection_name, data_json):
        """
        Permite insertar json a mongoDB

        ###**Parameters:**
        ----------
        * **collection_name (String):**
            nombre de la colección a la que se desea insertar los datos o la que se desea crear
        * **data_json (json):**
            json que se desea insertar
        
        ###**Returns:**
        ----------
        * **(String):**
            mensaje para notificarle al usuario si se pudo insertar o no los datos a la colección.

        ###**Example:**
        -----------
        ```python
        >>> import nucleoanalitico as na 
        >>> session = na.Conexion()
        >>> session.auth_mongo('name_db')
        >>> session.insert_json_to_mongo('name_collection', dict)
        ```   
        """
        self.collection_name = collection_name
        try:
            db = self.client[self.database]
            collection = db[self.collection_name]
            data_dict = json.loads(data_json)
            collection.insert_many(data_dict)
            logger.info("Los datos fueron insertados correctamente en la coleccion %s, de la base de datos %s." %(self.collection_name, self.database))
        
        except:
            logger.warning("Los datos NO fueron insertados en la coleccion %s, de la base de datos %s." %(self.collection_name, self.database))
        
    def extract_mongo(self, collection_name, multiple=False):
        """
        Extrae los datos de la collection especificada segun la base de datos

        ###**Parameters:**
        ----------
        * **collection_name (String):**
            nombre de la colección que desea extraer de la base de datos
        
        ###**Returns:**
        ----------
        * **(String):**
            mensaje para notificarle al usuario si se pudo extraer o no los datos a la colección.
        * **(DataFrame):**
            como objeto queda almacenado el dataframe con los datos de la base de datos despues de ser extraidos

        ###**Example:**
        -----------
        ```python
        >>> import nucleoanalitico as na 
        >>> session = na.Conexion()
        >>> session.auth_mongo('name_db')
        >>> session.extract_mongo('name_collection')
        >>> session.df
        ```   
        """
        self.collection_name = collection_name

        if  multiple == False:
            try:
                db = self.client[self.database]
                collection = db[self.collection_name]
                info = collection.find()
                df = pd.DataFrame(list(info))
                logger.info("Los datos de la coleccion %s de la base de datos %s fueron extraidos satisfactoriamente!" %(self.collection_name, self.database))
            
            except:
                logger.warning("Los datos de la coleccion %s de la base de datos %s NO fueron extraidos" %(self.collection_name, self.database))
            
            self.df = df
        
        else:
            output = {}

            for name in self.collection_name:
                try:
                    db = self.client[self.database]
                    collection = db[name]
                    info = collection.find()
                    df = pd.DataFrame(list(info))
                    output[name]=df
                    logger.info("Los datos de la coleccion %s de la base de datos %s fueron extraidos satisfactoriamente!" %(name, self.database))

                except:
                    logger.warning("Los datos de la coleccion %s de la base de datos %s NO fueron extraidos" %(name, self.database))
            

            self.df = output
        #return df

    def close_mongo(self):
        """
        Permite cerrar session en mongo

        ###**Returns:**
        ----------
        * **(String):**
            mensaje para notificarle al usuario si se pudo cerrar o no la conexion con la base de datos
        
        ###**Example:**
        -----------
        ```python
        >>> import nucleoanalitico as na 
        >>> session = na.Conexion()
        >>> session.auth_mongo('name_db')
        >>> session.close_mongo()
        ```   
        """
        try:
            self.client = pymongo.MongoClient()
            self.client.close()
            logger.info('La sesion de mongo se ha cerrado correctamente')
        
        except:
            logger.warning('La sesion de mongo NO se logro cerrar de manera satisfactoria')